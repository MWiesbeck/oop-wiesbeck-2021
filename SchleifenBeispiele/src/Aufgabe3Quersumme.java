import java.util.Scanner;

public class Aufgabe3Quersumme {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein");
		int quersumme = 0;
		int rest;
		int n = myScanner.nextInt();

		
		while(n >= 1)
		{
		  rest = n % 10;
		  quersumme = quersumme + rest;
		  n = n / 10;
		}
		 
		System.out.printf("Die Quersumme betr�gt: %d%n", quersumme);
	}

}
