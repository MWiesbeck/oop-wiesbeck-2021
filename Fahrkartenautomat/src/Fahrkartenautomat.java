﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       int anzahlTickets;
       int rückgabeInt;
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       System.out.print("Anzahl der Tickets: ");
       anzahlTickets = tastatur.nextInt();
       zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
      
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f Euro ==== " , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       rückgabeInt = (int)(100 * rückgabebetrag);
       if(rückgabebetrag > 0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro " , rückgabebetrag );
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabeInt >= 200) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
        	  rückgabeInt -= 200;
           }
           while(rückgabeInt >= 100) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
        	  rückgabeInt -= 100;
           }
           while(rückgabeInt >= 50) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
        	  rückgabeInt -= 50;
           }
           while(rückgabeInt >= 20) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
        	  rückgabeInt -= 20;
           }
           while(rückgabeInt >= 10) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
        	  rückgabeInt -= 10;
           }
           while(rückgabeInt >= 5)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
        	  rückgabeInt -= 5;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}